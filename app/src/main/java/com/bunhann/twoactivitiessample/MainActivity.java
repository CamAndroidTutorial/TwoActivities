package com.bunhann.twoactivitiessample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnSecondActivity;
    private EditText edMsg1;
    private TextView txtMsg1;

    private String msg1;

    public final static int EXTRA_REQUEST_CODE_MSG2 = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSecondActivity = (Button) findViewById(R.id.btnSecondActivity);
        edMsg1 = (EditText) findViewById(R.id.edMsg1);
        txtMsg1 = (TextView) findViewById(R.id.tvMsg1);

        btnSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg1 = edMsg1.getText().toString();
                if (msg1.length()>0){
                    Intent i = new Intent(v.getContext(), SecondActivity.class);
                    i.putExtra("MSG1", msg1);
                    //startActivity(i);
                    startActivityForResult(i, EXTRA_REQUEST_CODE_MSG2);
                } else
                    Toast.makeText(MainActivity.this, "Please enter data to send!", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EXTRA_REQUEST_CODE_MSG2) {
            if(resultCode == RESULT_OK) {
                String msg2 = data.getStringExtra("MSG2");
                txtMsg1.setText(msg2);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
