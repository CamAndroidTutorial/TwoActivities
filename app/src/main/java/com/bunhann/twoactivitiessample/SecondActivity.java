package com.bunhann.twoactivitiessample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private Button btnSendBack;
    private EditText edMsg2;
    private TextView txtMsg2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        btnSendBack = (Button) findViewById(R.id.btnSendBack);
        edMsg2 = (EditText) findViewById(R.id.edMsg2);

        txtMsg2 = (TextView) findViewById(R.id.tvMsg2);

        Intent i = getIntent();
        //Get via Bundle
        Bundle bundle = i.getExtras();
        String data = bundle.getString("MSG1");
        txtMsg2.setText(data);

        //String msg1 = i.getStringExtra("MSG1");

        btnSendBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg2 = edMsg2.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("MSG2", msg2);
                setResult(RESULT_OK, intent);
                onBackPressed();
            }
        });
    }
}
